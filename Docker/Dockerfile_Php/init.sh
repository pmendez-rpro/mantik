#!/bin/bash

#Configuraciones de PHP, dependientes del ambiente:
sed -i "s/{display_errors}/$PHP_DISPLAY_ERRORS/g" /usr/local/etc/php/php.ini
sed -i "s/{display_startup_errors}/$PHP_DISPLAY_ERRORS/g" /usr/local/etc/php/php.ini
sed -i "s/{enable_dl}/$PHP_ENABLE_DL/g" /usr/local/etc/php/php.ini
sed -i "s/{error_log}/$PHP_ERROR_LOG/g" /usr/local/etc/php/php.ini
sed -i "s/{log_errors}/$PHP_LOG_ERRORS/g" /usr/local/etc/php/php.ini
sed -i "s/{max_execution_time}/$PHP_MAX_EXECUTION_TIME/g" /usr/local/etc/php/php.ini

#Archivo de configuración:
rm -f /var/www/html/gestion/.env
mv /root/.env /var/www/html/gestion/.env
sed -i "s/{APP_ENV}/$LARAVEL_APP_ENV/g" /var/www/html/gestion/.env
sed -i "s/{APP_DEBUG}/$LARAVEL_APP_DEBUG/g" /var/www/html/gestion/.env

#Permisos:
chmod -R 777 /var/www/html

#Composer Update:
if [ "$COMPOSER_UPDATE" == "yes" ]; then
	cd /var/www/html/robot
	composer update
fi

php-fpm
