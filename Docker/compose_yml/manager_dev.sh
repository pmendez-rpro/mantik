#!/bin/bash

# Action					up|down
# Composer Update			yes|no

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

printf "${GREEN}Inicio del proceso de integración continua:${NC}\n"
set -e

# GET PARAMETERS ----------------------------------------------------------------
COMPOSE_YML_PATH="."
IS_ACT="0"
ENVIRONMENT="dev"
ACTION="up"
COMPOSER_UPDATE="no"

for ((i=1;i<=$#;i++));
	do
		if [ ${!i} = "-act" ]; then
			((i++))
			IS_ACT="1"
			ACTION=${!i}
		fi
		
		if [ ${!i} = "-composer-update" ]; then
			((i++))
			COMPOSER_UPDATE=${!i}
		fi
done;

printf "Parámetros:\n"
printf "IS_ACT: $IS_ACT\n"
printf "ENVIRONMENT: $ENVIRONMENT\n"
printf "ACTION: $ACTION\n"
printf "COMPOSER_UPDATE: $COMPOSER_UPDATE\n"

VALID="1"

if [ "$IS_ACT" == "0" ]; then
	printf "${RED}Se omitió el parámetro -act.${NC}\n"
	VALID="0"
fi

if [ "$ACTION" != "up" ] && [ "$ACTION" != "down" ]; then
	printf "${RED}El parámetro -act debe valer 'up' o 'down'.${NC}\n"
	VALID="0"
fi

if [ "$COMPOSER_UPDATE" != "yes" ] && [ "$COMPOSER_UPDATE" != "no" ]; then
	printf "${RED}El parámetro -composer-update debe valer 'yes' o 'no'.${NC}\n"
	VALID="0"
fi

# DOCKER COMPOSE CONF------------------------------------------------------------
if [ "$VALID" == "1" ]; then
	DOCKER_COMPOSE_YML="stack.yml"
	rm -f $DOCKER_COMPOSE_YML
	cp "stack_${ENVIRONMENT}.template.yml" $DOCKER_COMPOSE_YML
	sed -i "s/{COMPOSER_UPDATE}/$COMPOSER_UPDATE/g" $DOCKER_COMPOSE_YML
else
	printf "${RED}Proceso finalizado insatisfactoriamente, debido a parámetros inválidos.${NC}\n"
	exit 1
fi

# UP ACTION ----------------------------------------------------------------
if [ "$ACTION" == "up" ] && [ "$VALID" == "1" ]; then
	cp ${COMPOSE_YML_PATH}/mantik/mantik_${ENVIRONMENT}.env.template ${COMPOSE_YML_PATH}/mantik/mantik_${ENVIRONMENT}.env
	set -a
	source ${COMPOSE_YML_PATH}/mantik/mantik_${ENVIRONMENT}.env
	printf "${GREEN}Se inicia el Building de contenedores:${NC}\n"
	cat ${DOCKER_COMPOSE_YML} | envsubst | /usr/local/bin/docker-compose -f - -p mantik_${ENVIRONMENT} rm --stop --force
	cat ${DOCKER_COMPOSE_YML} | envsubst | /usr/local/bin/docker-compose -f - -p mantik_${ENVIRONMENT} up --build -d
	printf "${GREEN}Finalizó el Building de contenedores.${NC}\n"
	printf "${GREEN}Iniciando la verificación de contenedores:${NC}\n"
	CANTIDAD_CONTENEDORES="$(docker ps | wc -l)"
	if [ "$CANTIDAD_CONTENEDORES" != "2" ]; then
		printf "${RED}Uno o más contenedores se han detenido.${NC}\n"
		exit 1
	fi
	
	#Verifica PHP:
	printf "${GREEN}Verificando PHP:${NC}\n"
	while [ "$(docker exec mantik_${ENVIRONMENT}_php_1 bash -c 'ps ax | grep "php-fpm: master process" | grep -v "grep"')" == "" ]; do
    	sleep 1
		printf "."
	done
	printf "${GREEN}PHP iniciado.${NC}\n"
	
	printf "${GREEN}Finalizó la verificación de contenedores.${NC}\n"
	printf "${GREEN}¡Finalizó la integración continua! Verifique los escenarios ejecutados, para fiscalizar la corrección del proceso.${NC}\n"
fi

# DOWN ACTION ----------------------------------------------------------------

if [ "$ACTION" == "down" ] && [ "$VALID" == "1" ]; then
	set -a
	source ${COMPOSE_YML_PATH}/mantik/mantik_${ENVIRONMENT}.env
	printf "${GREEN}Se inicia la detención y eliminación de contenedores:${NC}\n"
	cat ${DOCKER_COMPOSE_YML} | envsubst | /usr/local/bin/docker-compose -f - -p mantik_${ENVIRONMENT} rm --stop --force
	printf "${GREEN}¡Finalizó la detención y eliminación de contenedores!${NC}\n"
fi
# -----------------------------------------------------------------------
