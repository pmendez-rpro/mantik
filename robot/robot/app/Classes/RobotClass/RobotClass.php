<?php
	namespace App\Classes\RobotClass;
	
	use \Exception;
	
	class RobotClass {
		//Constantes de errores:
		public const ERR_FILE_DOESNT_EXISTS = -1;
		public const ERR_INVALID_FORMAT = -2;
		public const ERR_WHILE_PROCESSING = -3;
		public const ERR_INVALID_FIRST_LINE = -4;
		public const ERR_INVALID_OBSTACLE = -5;
		public const ERR_INVALID_OBSTACLES_COUNT = -6;
		public const ERR_INVALID_ACTION = -7;
		public const ERR_INVALID_ACTIONS_COUNT = -8;
		public const ERR_OBSTACLE_AT_STARTING_POSITION = -9;
		
		//Desplazamientos que deben observarse, según los puntos cardinales:
		private $directions = [
			["x" => 0, "y" => 1], //Norte
			["x" => 1, "y" => 0], //Este
			["x" => 0, "y" => -1], //Sur
			["x" => -1, "y" => 0] //Oeste
		];
		
		private $current_direction = 0; //Inicia desde el norte.
		
		private $current_position = ["x" => 0, "y" => 0]; //Comienza en el origen de coordenadas.
		
		private $path; //Almacenerá la ruta del archivco por tratar.
		
		private $obstacles = []; //Almacenará las coordenadas de los obstáculos.
		
		private $actions = 0; //Contabilizará las acciones que deba realizar el robot.
		
		private $positions = [ //Guardará todas las posiciones y distancias recorridas, comenzando con la inicial.
			[
				"x" => 0,
				"y" => 0,
				"distance" => 0
			]
		];
		
		private $max_distance = 0; //Máxima distancia alcanzada hasta el momento.
		
		public function __construct(string $path) {
			$this->path = $path; //Almacena la ruta de archivo recibida.
		}
		
		public function move() { //Realiza la interpretación del archivo de instrucciones.
			if (!file_exists($this->path)) return self::ERR_FILE_DOESNT_EXISTS; //El archivo debe existir.
			try {
				$handler = fopen($this->path, "r"); //Abre el archivo para lectura línea por línea.
				$line = 1; //Primera línea.
				while(!feof($handler)) { //Mientras no se haya agotado el archivo...
					$text = str_replace(["\r", "\n"], ["", ""], fgets($handler)); //Lee la línea, pero removiendo sus caracteres de salto.
					if ($line == 1) { //Si se trata de la primera línea, debe indicar la cantidad de obstáculos y de instrucciones.
						$parts = explode(" ", $text); //Rompe la línea, esperando que conste de dos partes.
						//Si no posee dos partes o sus valores son anómalos, genera un error:
						if (count($parts) != 2 || $parts[0] != (int)$parts[0] || $parts[1] != (int)$parts[1]) return self::ERR_INVALID_FIRST_LINE;
						$parts[0] = (int)$parts[0]; //Conversión a entero.
						$parts[1] = (int)$parts[1]; //Conversión a entero.
						//Si la cantidad de obstáculos no está entre 1 y 10, o si la cantidad de acciones no se halla entre 1 y 10000, genera un error:
						if ($parts[0] < 1 || $parts[0] > 10 || $parts[1] < 1 || $parts[1] > 10000) return self::ERR_INVALID_FIRST_LINE;
					}
					else if ($line - 1 <= $parts[0]) { //Si se trata de un obstáculo.
						$obstacle = explode(" ", $text); //Rompe la línea, procurando hallar dos componentes.
						//Si no posee dos partes o sus valores son anómalos, genera un error:
						if (count($obstacle) != 2 || $obstacle[0] != (int)$obstacle[0] || $obstacle[1] != (int)$obstacle[1]) return self::ERR_INVALID_OBSTACLE;
						$obstacle[0] = (int)$obstacle[0]; //Conversión a entero.
						$obstacle[1] = (int)$obstacle[1]; //Conversión a entero.
						//Si las coordenadas del obstáculo no se hallan entre -100000 y 100000, se genera un error:
						if ($obstacle[0] < -100000 || $obstacle[0] > 100000 || $obstacle[1] < -100000 || $obstacle[1] > 100000) return self::ERR_INVALID_OBSTACLE;
						//Si un obstáculo se encuentra en el origen de coordenadas, el robot no podría desplazarse:
						if ($obstacle[0] == 0 && $obstacle[1] == 0) return self::ERR_OBSTACLE_AT_STARTING_POSITION;
						//Evita guardar obstáculos duplicados:
						if (!in_array($obstacle, $this->obstacles)) $this->obstacles[] = $obstacle;
					}
					else {
						//Acciones:
						//Si la cantidad de obstáculos no coincide con lo declarado en la primera línea, genera un error:
						if (count($this->obstacles) != $parts[0]) return self::ERR_INVALID_OBSTACLES_COUNT;
						$action = explode(" ", $text); //Verifica si la acción puede fragmentarse en dos partes.
						switch(count($action)) {
							case 1: //Posee una sola parte.
								switch($action[0]) {
									case "L": //Moverse a la izquierda.
										$this->current_direction--; //Concreta el retroceso.
										if ($this->current_direction == -1) $this->current_direction = 3; //Posible rectificación.
										break;
									case "R": //Moverse a la derecha.
										$this->current_direction++; //Concreta el avance.
										if ($this->current_direction == 4) $this->current_direction = 0; //Posible rectificación.
										break;
									default: //No se reconoce el valor especificado.
										return self::ERR_INVALID_ACTION;
								}
								break;
							case 2: //Posee dos partes. La primera debe ser una "M":
								//Si el primer elemento no es una "M" o el valor que la acompaña es inválido:
								if ($action[0] != "M" || $action[1] != (int)$action[1]) return self::ERR_INVALID_ACTION;
								$action = (int)$action[1]; //Conversión a entero.
								//Si el desplazamiento no está entre 1 y 10, genera un error:
								if ($action < 1 || $action > 10) return self::ERR_INVALID_ACTION;
								//Toma la posición actual:
								$position = $this->positions[count($this->positions) - 1];
								
								//X e Y:
								foreach (["x", "y"] as $coord) { //Recorre ambos ejes:
									$axis = $position[$coord]; //Valor que posee actualmente el eje que se está tratando.
									switch($this->directions[$this->current_direction][$coord]) { //Según el desplazamiento:
										case 0: //No realizar ninguna acción.
											continue 2;
										case -1: //Retroceso.
											//Retrocederá tanto como se indique o sea posible:
											for ($l = $position[$coord] - 1; $l >= $position[$coord] - $action; $l--) {
												if ($l < -100000) break; //No se puede exceder este límite.
												//Posición a la que se trasladaría el robot:
												$pair = ($coord == "x") ? [$l, $position["y"]] : [$position["x"], $l];
												if (!in_array($pair, $this->obstacles)) {
													//Si no hay un obstáculo allí, da por válido el desplazamiento:
													$axis = $l;
												}
												else {
													//De lo contrario, no prosigue con el desplazamiento:
													break;
												}
											}
											break;
										case 1:
											//Avanzará tanto como se indique o sea posible:
											for ($l = $position[$coord] + 1; $l <= $position[$coord] + $action; $l++) {
												if ($l > 100000) break; //No se puede exceder este límite.
												//Posición a la que se trasladaría el robot:
												$pair = ($coord == "x") ? [$l, $position["y"]] : [$position["x"], $l];
												if (!in_array($pair, $this->obstacles)) {
													//Si no hay un obstáculo allí, da por válido el desplazamiento:
													$axis = $l;
												}
												else {
													//De lo contrario, no prosigue con el desplazamiento:
													break;
												}
											}
											break;
									}
									$position[$coord] = $axis; //Esta será la nueva posición del robot, para la coordenada dada.
									//Cálculo de la distancia euclidiana:
									$position["distance"] = round(sqrt(pow($position["x"], 2) + pow($position["y"], 2)), 2);
									//Si esta es la mayor distancia registrada hasta el momento, la computa:
									if ($position["distance"] > $this->max_distance) $this->max_distance = $position["distance"];
									
									$this->positions[] = $position; //Agrega la nueva posición.
								}
								
								break;
							default: //Una acción debe poseer una o dos partes.
								return self::ERR_INVALID_ACTION;
						}
						$this->actions++; //Se computa una nueva acción.
					}
					$line++; //Se computa una nueva línea del archivo procesada.
				}
				if ($line == 1) return self::ERR_INVALID_FORMAT; //No puede aceptarse que el archivo posea una sola línea.
				//Si la cantidad de obstáculos no coincide con lo declarado en la primera línea, se genera un error:
				if (count($this->obstacles) != $parts[0]) return self::ERR_INVALID_OBSTACLES_COUNT;
				//Si la cantidad de acciones o comandos no coincide con lo declarado en la primera línea, se genera un error:
				if ($this->actions != $parts[1]) return self::ERR_INVALID_ACTIONS_COUNT;
				//print_r($this->positions);
				
				return $this->max_distance; //Retorna la máxima distancia alcanzada.
			}
			catch(Exception $e) {
				return self::ERR_WHILE_PROCESSING; //Cualquier otro tipo de error que se haya producido.
			}
    	}
	}
?>