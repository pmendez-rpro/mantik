<?php
    namespace App\Console\Commands;
    
    use Illuminate\Console\Command;
	use App\Classes\RobotClass\RobotClass; //Clase que resolverá la caminata del robot.
    
    class Robot extends Command {
		//cd /var/www/html/robot && php artisan robot:robot --path="RUTA DEL ARCHIVO POR PROCESAR"
		//cd /var/www/html/robot && php artisan robot:robot --path=/var/www/html/robot/app/Console/Commands/example.txt
		
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = "robot:robot {--path=}";

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Calcula la distancia euclidiana a la que se hallará un robot, tras recorrer un determinado camino.';
        
        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct() {
            parent::__construct();
        }
        
        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle() {
            $options = $this->options(); //Obtiene las opciones.
			$path = $options["path"] ?? ""; //Obtiene la ruta del archivo por tratar.
			$robot = new RobotClass($path); //Instancia de RobotClass y pasa la ruta al constructor.
			$result = $robot->move(); //Pone en marcha el movimiento del robot.
			if ($result < 0) { //Si se obtuvo un código de error, lo indica.
				echo "Error code $result";
			}
			else { //De lo contrario, comunica la máxima distancia alcanzada.
				echo "La mayor distancia es $result";
			}
        }
    }
?>